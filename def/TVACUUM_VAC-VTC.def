########################### Vacuum TC Control			############################
########################### Version: 4.0.1           			 ############################


#-############################
#- COMMAND BLOCK
#-############################

define_command_block()

add_digital("ResetCmd",                        PV_NAME="RstErrorsCmd",                  PV_DESC="Reset errors and warnings")
#-----------------------------
add_digital("ClearCountersCmd",                PV_NAME="ClrCountersCmd",                PV_DESC="Clear counters")

#-############################
#- STATUS BLOCK
#-############################

define_status_block()

add_analog("Temperature",              "REAL",     PV_NAME="TemperatureR",                      PV_DESC="Sensor value", PV_EGU="C", ARCHIVE=True)

add_digital("ErrorSt",                         PV_NAME="ErrorR",                       	 PV_DESC="Error detected by the control function",       PV_ONAM="Error", ARCHIVE=True)
add_digital("OffSt",                           PV_NAME="DisableR",                     	 PV_DESC="The thermocouple STOPPED",                     PV_ONAM="Stopped")
add_digital("OnSt",                            PV_NAME="EnableR",                        PV_DESC="The thermoCouple is ON",   			 PV_ONAM="On")
add_digital("WarningSt",                       PV_NAME="WarningR",                     	 PV_DESC="A warning is active",                    	 PV_ONAM="Warning")
add_digital("ValidSt",                         PV_NAME="ValidR",                       	 PV_DESC="Communication is valid",                 	 PV_ONAM="Valid",                PV_ZNAM="Invalid")
add_digital("UndefinedSt",                     PV_NAME="UndefinedR",                     PV_DESC="Communication is valid",                 	 PV_ONAM="Valid",                PV_ZNAM="Invalid")

add_analog("WarningCode",          "BYTE",     PV_NAME="WarningCodeR",                  PV_DESC="Active warning code")

add_analog("ErrorCode",            "BYTE",     PV_NAME="ErrorCodeR",                    PV_DESC="Active error code", ARCHIVE=True)

add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:#StatR")
{
field(SCAN, "1 second")
	field(DESC, "Calculate TC status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:UndefinedR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:EnableR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:DisableR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ErrorR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:ValidR CP MSS")

# Recalculate 'undefined' (incorrect in PLC)
	field(CALC, "A:=(B==C);F:=(A || B || C || D);F")
	field(OCAL, "E&&F?(D?3:(C?2:(B?1:0))):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:StatR PP MSS")

}

record(mbbi, "[PLCF#INSTALLATION_SLOT]:StatR")
{
	field(DESC, "Thermocouple status")
	field(ZRST, "INVALID")
	field(ONST, "ON")
	field(TWST, "OFF")
	field(THST, "ERROR")
}




""")


#-############################
#- METADATA
#-############################
#- ERRORS
#-define_metadata("error", MD_SCRIPT='css_code2msg.py', CODE_TYPE='error')
#-
#-add_metadata(99, 'hardware error')
#-
#- WARNINGS
#-define_metadata("warning", MD_SCRIPT='css_code2msg.py', CODE_TYPE='warning')
#-
#-add_metadata(99, '')
